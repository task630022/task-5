using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services;
using CustomerApi.Models;

namespace CustomerApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CustomerController : ControllerBase
{
    private readonly CustomerServices customerServices;

    public CustomerController(CustomerServices service) {
        customerServices = service;
    }

    [HttpPost]
    public ActionResult AddCustomer([FromBody] Customer data)
    {
        if (data.Firstname == null ||data.Lastname == null) {
            return BadRequest("Data Customer tidak lengkap");
        }
        try {
            customerServices!.add(data);
            return CreatedAtAction(nameof(GetCustomer), new { id = data.Id }, data);
        } 
        catch {
            return NotFound("Customer tidak ditemukan.");
        }
        
    } 

    [HttpGet]
    public ActionResult<List<Customer>> GetCustomers()
    {
        try {
            var customers = customerServices.get();
            return Ok(customers);
        } catch {
            return NotFound("Customer tidak ditemukan.");
        }
    }

    [HttpGet("{id}")]
    public ActionResult<Customer> GetCustomer(int id)
    {
        try {
            var customer = customerServices.get(id: id);
            return Ok(customer);
        }
        catch  {
            return NotFound("Customer tidak ditemukan.");
        }
    }

    [HttpPut("{id}")]
    public ActionResult UpdateConsumer(int id, Customer data)
    {
        try {
            customerServices.update(id, data);
            return Ok($"Customer Id: {data.Id} telah berhasil di edit");
        }
        catch {
            return NotFound("Customer tidak ditemukan.");
        }
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteCustomer(int id)
    {
        try {
            customerServices.delete(id);
            return Ok($"Customer Id: {id} telah berhasil di hapus");
        }
        catch {
            return NotFound("Customer tidak ditemukan.");
        }
    }


}