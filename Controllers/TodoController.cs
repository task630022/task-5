using Microsoft.AspNetCore.Mvc;
using CustomerApi.Services;

namespace CustomerApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TodoController : ControllerBase 
{

    private readonly TodoServices todoServices;

    public TodoController(TodoServices service) {
        todoServices = service;
    }

    [HttpGet]
    public async Task<IActionResult> GetTodos()
    {
        try
        {
            var todos = await todoServices!.GetTodosAsync();
            return Ok(todos);
        }
        catch (Exception e) {
            return Problem(e.Message);
        }
    }

    [HttpGet("{id}")]
    public IActionResult GetTodo(int id) 
    {
        try {
            var todo = todoServices!.GetTodo(id);
            return Ok(todo);
        } catch (Exception e) {
            return Problem(e.Message);
        }
    }


}
