using CustomerApi.Data;
using CustomerApi.Services;
using Microsoft.EntityFrameworkCore;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Setup Logger
const string logPath = "../log/runtime.log";
builder.Host.UseSerilog((hostContext, services, configuration) => {
    configuration
    .WriteTo.Console()
    .WriteTo.File(logPath, rollingInterval: RollingInterval.Day);
});

// Add services to the container.
builder.Services.AddHttpClient<TodoServices>();
builder.Services.AddScoped<CustomerServices>();
builder.Services.AddControllers();

// Add EF DB context
builder.Services.AddDbContext<CustomerDBContext>(
    options => options.UseNpgsql(
        builder.Configuration.GetConnectionString("DefaultConnection")
    )
);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
