﻿using Microsoft.EntityFrameworkCore;
using CustomerApi.Models;

namespace CustomerApi.Data;

public partial class CustomerDBContext : DbContext
{
    public CustomerDBContext() {}

    public CustomerDBContext(DbContextOptions<CustomerDBContext> options) : base(options) {}

    public virtual DbSet<Customer>? Customers { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Customer>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("customer_pkey");
            entity.ToTable("customer");
            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Email).HasMaxLength(100).HasColumnName("email");
            entity.Property(e => e.Firstname).HasMaxLength(50).HasColumnName("firstname");
            entity.Property(e => e.Homeaddress).HasMaxLength(200).HasColumnName("homeaddress");
            entity.Property(e => e.Lastname).HasMaxLength(50).HasColumnName("lastname");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
