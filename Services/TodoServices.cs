using CustomerApi.Models;

namespace CustomerApi.Services;

public class TodoServices 
{
    private readonly HttpClient httpClient;

    public TodoServices(HttpClient client) 
    {
        httpClient = client;
        httpClient.BaseAddress = new Uri("https://jsonplaceholder.typicode.com/");
    }

    public async Task<IEnumerable<Todo>?> GetTodosAsync()
    {
        var todos = await httpClient.GetFromJsonAsync<IEnumerable<Todo>>("todos");
        return todos;
    }

    public Todo? GetTodo(int todoId)
    {
        var todo = httpClient.GetFromJsonAsync<Todo>($"todos/{todoId}").Result;
        return todo;
    }
}