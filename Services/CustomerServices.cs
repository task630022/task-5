using CustomerApi.Data;
using CustomerApi.Models;

namespace CustomerApi.Services;

public class CustomerServices 
{
    
    private readonly CustomerDBContext dBContext;

    public CustomerServices(CustomerDBContext context)
    {
        dBContext = context;
    }

    public void add(Customer data) 
    {
        dBContext.Add(data);
        dBContext.SaveChanges();
    }

    public List<Customer> get() => dBContext.Customers!.ToList();

    public Customer? get(int id) => dBContext.Customers?.Single(e => e.Id == id);

    public void update(int id, Customer data) {
        try {
            var entry = dBContext.Customers!.Find(id);
            entry!.Firstname = data.Firstname;
            entry!.Lastname = data.Lastname;
            entry!.Email = data.Email;
            entry!.Homeaddress = data.Homeaddress;
            dBContext.SaveChanges();
        } 
        catch (Exception e) {
            throw e;
        }
    }

    public void delete(int id)
    {
        try {
            var entry = dBContext.Customers!.Find(id);
            dBContext.Customers.Remove(entry!);
            dBContext.SaveChanges();
        }
        catch (Exception e) {
            throw e;
        }
    }
    
}